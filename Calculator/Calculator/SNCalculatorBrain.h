//
//  SNCalculatorBrain.h
//  Calculator
//
//  Created by Suneil Nyamathi on 11/24/12.
//  Copyright (c) 2012 snyamathi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SNCalculatorBrain : NSObject {
    double operand;
    NSString * waitingOperation;
    double waitingOperand;
}

- (void)setOperand:(double)anOperand;
- (double)performOperation:(NSString *)operation;

@end
