//
//  SNViewController.h
//  Calculator
//
//  Created by Suneil Nyamathi on 11/24/12.
//  Copyright (c) 2012 snyamathi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SNCalculatorBrain.h"

@interface SNViewController : UIViewController {
    IBOutlet UILabel *display;
    SNCalculatorBrain *brain;
    BOOL userIsInTheMiddleOfTypingANumber;
}

- (IBAction)digitPressed:(UIButton *)sender;
- (IBAction)operationPressed:(UIButton *)sender;

@end
