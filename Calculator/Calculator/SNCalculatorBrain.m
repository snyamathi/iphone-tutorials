//
//  SNCalculatorBrain.m
//  Calculator
//
//  Created by Suneil Nyamathi on 11/24/12.
//  Copyright (c) 2012 snyamathi. All rights reserved.
//

#import "SNCalculatorBrain.h"

@implementation SNCalculatorBrain

- (void)performWaitingOperation
{
    if ([waitingOperation isEqual:@"+"]) {
        operand = waitingOperand + operand;
    } else if ([waitingOperation isEqual:@"-"]) {
        operand = waitingOperand - operand;
    } else if ([waitingOperation isEqual:@"*"]) {
        operand = waitingOperand * operand;
    } else if ([waitingOperation isEqual:@"/"]) {
        if (operand) {
            operand = waitingOperand / operand;
        }
    }
}

- (void)setOperand:(double)anOperand
{
    operand = anOperand;
}

- (double)performOperation:(NSString *)operation
{
    if ([operation isEqual:@"sqrt"]) {
        operand = sqrt(operand);
    } else {
        [self performWaitingOperation];
        waitingOperation = operation;
        waitingOperand = operand;
    }
    return operand;
}

@end