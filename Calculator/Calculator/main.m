//
//  main.m
//  Calculator
//
//  Created by Suneil Nyamathi on 11/24/12.
//  Copyright (c) 2012 snyamathi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SNAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SNAppDelegate class]));
    }
}
