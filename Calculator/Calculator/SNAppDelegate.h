//
//  SNAppDelegate.h
//  Calculator
//
//  Created by Suneil Nyamathi on 11/24/12.
//  Copyright (c) 2012 snyamathi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
